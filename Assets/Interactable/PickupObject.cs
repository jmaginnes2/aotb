﻿using UnityEngine;

public class PickupObject : MonoBehaviour
{
    public GameObject _weaponSlot;
    public float _keyDelay = 1f;
    private float _timePassed = 0f;
    private bool _inRange = false;

    private void OnTriggerEnter(Collider collider)
    {
       if(collider.gameObject.tag == "Player")
        {
            _inRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _inRange = false;
    }

    void Update()
    {
        _timePassed += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.E) && _inRange && _timePassed >= _keyDelay)
        {
            if(gameObject.tag == "Weapon")
            {
                OnWeaponSelected();
            }
        }
    }


    private void OnWeaponSelected()
    {
        _timePassed = 0f;

        GameObject player = GameObject.FindWithTag("Player");
        PlayerInventory playerInventory = player.GetComponent("PlayerInventory") as PlayerInventory;
        print(playerInventory._currentWeapon);

        if (playerInventory != null)
        {
            if (playerInventory._currentWeapon != null)
            {
                if (playerInventory._currentWeapon != gameObject.name)
                {
                    Swap();
                }
            }

            playerInventory.Equip(gameObject.name);

        }
        gameObject.transform.SetParent(_weaponSlot.transform, false);
        gameObject.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
        gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
        gameObject.transform.localScale = new Vector3(2f, 2f, 2f);
    }

    // will need to fix this function once we can pickup things other than just weapons but for now it is fine

    private void Swap()
    {
        Transform child = _weaponSlot.transform.GetChild(0);
        print(_weaponSlot.transform.childCount);
        _weaponSlot.transform.DetachChildren();
        child.transform.position = gameObject.transform.position;
        Collider dropped = child.GetComponent("MeshCollider") as Collider;
        print(dropped);
        dropped.isTrigger = true;
    }

}