﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment
{
    public Dictionary<string, float> _weapons = new Dictionary<string, float>();
    
    public Equipment()
    {
        // set up possible values
       _weapons.Add("SM_Wep_Cutlass_01", 15f);
       _weapons.Add("SM_Wep_Sabre_01", 20f);

    }


}
