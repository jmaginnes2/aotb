﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResources 
{
    void Increase(float val);
    void Decrease(float val);
}
