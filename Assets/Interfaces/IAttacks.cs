﻿using UnityEngine;

public interface IAttacks
{
    string OnLightAttack(Vector3 dir);
    string OnHeavyAttack(Vector3 dir);
}
