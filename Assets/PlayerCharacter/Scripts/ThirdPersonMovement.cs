﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class ThirdPersonMovement : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public Animator animator;
    private Vector3 rollingDir;
    private IAttacks _attacks;


    public float speed = 6f;
    public float turnSmooth = 0.1f;
    float turnSmoothVel;

    private void Awake()
    {
        _attacks = GetComponent<IAttacks>();
    }

    // Update is called once per frame
    // this update will read all player inputs and respond accordingly
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 dir = new Vector3(horizontal, 0f, vertical).normalized;
        animator.SetFloat("Speed", dir.sqrMagnitude);
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("DivingRoll"))
        {
            dir = rollingDir;
        }
 
        if (dir.magnitude >= 0.1 && !(animator.GetCurrentAnimatorStateInfo(0).IsTag("Stationary")))
        {
            float targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVel, turnSmooth);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDir.normalized * speed * Time.deltaTime);


            if (Input.GetKeyDown(KeyCode.Space))
            {
                animator.SetTrigger("Dive");
                rollingDir = dir;
            }

        }
            
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            string attack = _attacks.OnLightAttack(dir);
            if(attack != null)
            {
                animator.SetTrigger(attack);
                
            }
        }
    }


    
}
