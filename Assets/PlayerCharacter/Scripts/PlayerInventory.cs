﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    // Start is called before the first frame update
    private Equipment equipment;
    public string _currentWeapon = null;
    public float _currentAttackPower = 0f;

    void Start()
    {
        equipment = new Equipment();
        _currentWeapon = null;
        _currentAttackPower = 0;
    }

    public void Equip(string weaponName)
    {
        if (weaponName != null)
        {
            _currentWeapon = weaponName;
        }

        if (equipment._weapons.ContainsKey(weaponName))
        {
            _currentAttackPower = equipment._weapons[weaponName];
        }

    }
}
