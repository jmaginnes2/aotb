﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAttack : MonoBehaviour
{
    public float _keyDelay = .25f;
    private float _timePassed = 0f;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Enemy")
        {
            PlayerInventory playerInventory = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerInventory>();
            Animator animator = GameObject.FindGameObjectsWithTag("Player")[0].GetComponentInChildren<Animator>();
            // TODO make sure the player is actually attacking otherwise he could inflict damage just by running into the player
            bool isAttacking = animator.GetCurrentAnimatorStateInfo(0).IsTag("Stationary") || animator.GetCurrentAnimatorStateInfo(0).IsTag("Moving");
            if (playerInventory._currentWeapon == gameObject.name && _timePassed >= _keyDelay && isAttacking)
            {
                _timePassed = 0;
                EnemyHealth enemyHealth = collider.gameObject.GetComponent<EnemyHealth>();
                float power = playerInventory._currentAttackPower;
                float upper = (power * .2f) + power;
                float lower = power - (power * .2f);
                float hitAmount = Random.Range(lower, upper);
                print("Hit for " + hitAmount.ToString());
                enemyHealth._health.Decrease(playerInventory._currentAttackPower);

            }
        }
    }

    private void Update()
    {
        _timePassed += Time.deltaTime;
    }

}
