﻿using UnityEngine;

class PlayerAttacks : MonoBehaviour, IAttacks
{

    private float _timePassed = 0f;
    public float _keyDelay = .25f;

    private void Update()
    {
        _timePassed += Time.deltaTime;
    }
    public string OnLightAttack(Vector3 dir)
    {
        if (_timePassed >= _keyDelay)
        {
            _timePassed = 0;
            if (dir.magnitude >= 0.01)
            {
                print("here");
                return "LightAttackTwo";
                
            }
            else
            {
                return "LightAttackOne";
            }

        }

        else
        {
            return null;
        }
    }

    
    // stubbed
    public string OnHeavyAttack(Vector3 dir)
    {
         return null;
    }

}

