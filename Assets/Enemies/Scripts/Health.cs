﻿using UnityEngine;

public class Health : MonoBehaviour, IResources
{
    public float _currentHealth;
    public void Increase(float val)
    {
        _currentHealth += val;
    }

    public void Decrease(float val)
    {
        _currentHealth -= val;
    }
}

