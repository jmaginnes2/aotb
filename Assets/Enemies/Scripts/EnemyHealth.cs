﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour 
{
    // Start is called before the first frame update
    public Health _health;
    void Start()
    {
        _health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_health._currentHealth <= 0)
        {
            print("player has died");
            Destroy(gameObject);
        }
    }
}

